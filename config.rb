css_dir = "css"
sass_dir = "scss"
javascripts_dir = [
    # "node_modules/jquery.waitforimages/dist", 
    "js"
    ]
fonts_dir = "fonts"
images_dir = "images"

relative_assets = true

environment = :development
output_style = (environment == :production) ? :expanded : :nested
# line_comments = (environment == :production) ? false : true