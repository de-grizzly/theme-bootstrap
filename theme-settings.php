<?php

/**
 * @file
 * Theme settings form for Bootstrap Sub-theme theme.
 */

/**
 * Implements hook_form_system_theme_settings_alter().
 */
function bootstrap_sub_theme_form_system_theme_settings_alter(&$form, &$form_state) {

  $form['bootstrap_sub_theme'] = [
    '#type' => 'details',
    '#title' => t('Bootstrap Sub-theme'),
    '#open' => TRUE,
  ];

  $form['bootstrap_sub_theme']['font_size'] = [
    '#type' => 'number',
    '#title' => t('Font size'),
    '#min' => 12,
    '#max' => 18,
    '#default_value' => theme_get_setting('font_size'),
  ];

}
